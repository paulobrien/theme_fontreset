package uk.co.olabs.theme.ps;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    static String TAG = "THEME";

    String targetDirectory = Environment.getExternalStorageDirectory() + File.separator + "HWThemes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onInstallThemeClick(View v){

        // Set up the directory
        File directory = new File(targetDirectory);

        // Extract the theme(s)!
        extractThemes(directory);

        // Tell the user how they should set the theme active
        Toast.makeText(getApplicationContext(), "Now select the " + getResources().getString(R.string.app_name) + " theme in the 'Mine' tab.", Toast.LENGTH_LONG).show();

        // Launch the Huawei Theme Manager app so they can do so
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.huawei.android.thememanager");
        startActivity(launchIntent);

        // Byeee!
        finish();
    }

    private void extractThemes(File directory) {

        // Might be the first non built-in theme, so make the dir
        directory.mkdirs();

        // Get the assets
        AssetManager assetManager = getAssets();
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e(TAG, "Failed to get the asset file list.", e);
        }

        // For the hwt files, extract the assets to the created directory
        if (files != null) for (String filename : files) {
            if (filename.endsWith(".hwt"))
            {
                InputStream in = null;
                OutputStream out = null;
                try {
                    in = assetManager.open(filename);
                    File outFile = new File(Environment.getExternalStorageDirectory() + File.separator + "HWThemes", filename);
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                } catch (IOException e) {
                    Log.e(TAG, "Failed to copy the asset file " + filename, e);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                }
            }
        }
    }

    // Copy the file, InputStream to OutputStream - simples
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
}
